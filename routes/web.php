<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/categories', [App\Http\Controllers\HomeController::class, 'index_category'])->name('home.category');

Route::get('/blog/{blogPost}', [\App\Http\Controllers\PostController::class, 'show']);
Route::get('/blog/create/post', [\App\Http\Controllers\PostController::class, 'create']); //shows create post form
Route::post('/blog/create/post', [\App\Http\Controllers\PostController::class, 'store']); //saves the created post to the databse
Route::get('/blog/{blogPost}/edit', [\App\Http\Controllers\PostController::class, 'edit']); //shows edit post form
Route::put('/blog/{blogPost}/edit', [\App\Http\Controllers\PostController::class, 'update']); //commits edited post to the database 
Route::get('/blog/remove/{id}', [\App\Http\Controllers\PostController::class, 'destroy'])->name('post.delete'); //deletes post from the database

Route::get('/blog/category/{blogCategory}', [\App\Http\Controllers\PostCategoryController::class, 'show']);
Route::get('/blog/category/create/new', [\App\Http\Controllers\PostCategoryController::class, 'create']); 
Route::post('/blog/category/create/new', [\App\Http\Controllers\PostCategoryController::class, 'store']); 
Route::get('/blog/category/{blogCategory}/edit', [\App\Http\Controllers\PostCategoryController::class, 'edit']); 
Route::put('/blog/category/{blogCategory}/edit', [\App\Http\Controllers\PostCategoryController::class, 'update']); 
Route::get('/blog/category/remove/{id}', [\App\Http\Controllers\PostCategoryController::class, 'destroy'])->name('category.delete');


Auth::routes();

