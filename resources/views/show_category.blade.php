@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if (Auth::user()->is_admin())
            Admin
        @else
            User
        @endif
    &nbsp;Dashboard
    </h1>
@stop

@section('content')
    <p>Welcome, {{ Auth::user()->name }} !</p>

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/home/categories" class="btn btn-outline-primary btn-sm">Go back</a>
                <h1 class="display-one">Category Name: {{ ucfirst($category->name) }}</h1>
                <p>Assigned posts count: {{ $posts_assign }}</p> 
                <hr>
                
                <a href="/blog/category/{{ $category->id }}/edit" class="btn btn-outline-primary">Edit Category</a>
                <br><br>
                <form id="delete-frm" class="" action="" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger">Delete Category</button>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
