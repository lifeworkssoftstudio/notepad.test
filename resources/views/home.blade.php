@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if (Auth::user()->is_admin())
            Admin
        @else
            User
        @endif
    &nbsp;Dashboard
    </h1>
@stop

@section('content')
    <p>Welcome, {{ Auth::user()->name }} !</p>

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                @forelse($posts as $post)
                    <ul>
                        <li>

                                @if (Auth::user()->is_admin() && $post->active)
                                    <i class="far fa-eye"></i>
                                @elseif ( (Auth::user()->is_admin() && !$post->active))
                                    <i class="far fa-eye-slash"></i>
                                @endif
                                <a href="./blog/{{ $post->id }}">{{ ucfirst($post->name) }}</a>
                                <a href="./blog/{{ $post->id }}/edit">[ Edit ]</a>
                            
                                <a href="{{ route('post.delete', $post->id) }}">[ Delete ]</a>

                        </li>
                    </ul>
                @empty
                    <p class="text-warning">No blog Posts available</p>
                @endforelse
            </div>

            <div class="col-4">
                <a href="/blog/create/post" class="btn btn-primary btn-sm">Add New Post</a>
            </div>
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
