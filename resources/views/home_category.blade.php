@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if (Auth::user()->is_admin())
            Admin
        @else
            User
        @endif
    &nbsp;Dashboard
    </h1>
@stop

@section('content')
    <p>Welcome, {{ Auth::user()->name }} !</p>

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                @forelse($cats as $one)
                    <ul>
                        <li>    
                                {{ ucfirst($one->name) }} ( {{ count($one->posts) }} )
                                @if ($one->name!='Default')
                                &nbsp;&nbsp;
                                
                                <a href="/blog/category/{{ $one->id }}/edit">[ Edit ]</a>
                            
                                <a href="{{ route('category.delete', $one->id) }}" onclick="return do_delete_not_empty({{ $one->id }});">[ Delete ]</a>
                                @endif
                        </li>
                    </ul>
                @empty
                    <p class="text-warning">No blog Categories available</p>
                @endforelse
            </div>

            <div class="col-4">
                <a href="/blog/category/create/new" class="btn btn-primary btn-sm">Add New Category</a>
            </div>
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> 
    // $('#delete-frm-{{ $one->id }}').submit()
    function do_delete_not_empty(cat_id){
        return confirm("Are you sure want to delete category?")
    }
    </script>
@stop
