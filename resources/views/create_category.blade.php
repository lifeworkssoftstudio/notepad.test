@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if (Auth::user()->is_admin())
            Admin
        @else
            User
        @endif
    &nbsp;Dashboard
    </h1>
@stop

@section('content')
    <p>Welcome, {{ Auth::user()->name }} !</p>

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/home/categories" class="btn btn-outline-primary btn-sm">Go back</a>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h1 class="display-4">Create a New Category</h1>
                    <p>Fill and submit this form to create a Category</p>
                    <hr>

                    <form action="" method="POST">
                        @csrf
                        <div class="row">
                            <div class="control-group col-12">
                                <label for="title">Category Name</label>
                                <input type="text" id="title" class="form-control" name="title"
                                       placeholder="Enter Category Title" required>
                            </div>
                            
                        </div>
                        <div class="row mt-2">
                            <div class="control-group col-12 text-center">
                                <button id="btn-submit" class="btn btn-primary">
                                    Create Category
                                </button>                             
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
