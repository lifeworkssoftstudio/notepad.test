@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if (Auth::user()->is_admin())
            Admin
        @else
            User
        @endif
    &nbsp;Dashboard
    </h1>
@stop

@section('content')
    <p>Welcome, {{ Auth::user()->name }} !</p>

    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/home" class="btn btn-outline-primary btn-sm">Go back</a>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h1 class="display-4">Edit Post</h1>
                    <p>Edit and submit this form to update a post</p>

                    <hr>

                    <form action="" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="control-group col-12">
                                <label for="title">Post Title</label>
                                <input type="text" id="title" class="form-control" name="title"
                                       placeholder="Enter Post Title" value="{{ $post->name }}" required>
                            </div>
                                                      
                            <div class="control-group col-12 mt-2">
                                <label for="body">Post Body</label>
                                <textarea id="body" class="form-control" name="body" placeholder="Enter Post Body"
                                          rows="5" required>{{ $post->content }}</textarea>
                            </div>
                        </div>
                        <div class="control-group col-6">
                                <label for="category">Post Category</label>
                                <select name="category[]" id="category" class="form-control" multiple required>
                                    
                                    @foreach($cats as $cat)
                                        
                                        <option 
                                        @if (in_array($cat->id, $cat_ids))
                                         selected
                                        @endif
                                        value="{{ $cat->id }}">{{ $cat->name }}</option>

                                    @endforeach

                                </select>
                        </div>
                        <div class="row mt-2">
                            @if (Auth::user()->is_admin())
                                <div class="control-group col-6">
                                    <label for="active">Post is Active?</label>
                                    <input type="checkbox" id="active" {{ $is_active }} class="form-control text-right" name="is_active">
                                </div>
                            @endif
                            <div class="control-group col-12 text-center">
                                <button id="btn-submit" class="btn btn-primary">
                                    Update Post
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
