<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function create()
    {
        $categoryes = PostCategory::all();
        return view('create_post')->with('cats', $categoryes);
    }

    public function store(Request $request)
    {
        $cat_ids = [];
        foreach ($request->category as $cid){
            $cat_ids[] = $cid;
        }
        
        $newPost = Post::create([
            'name' => $request->title,
            'content' => $request->body,
            'author_id' => Auth::id(),
        ]);
        $newPost->categories()->sync($cat_ids);

        return redirect('home');
    }

    public function show(Post $blogPost)
    {
        $cats='';
        foreach($blogPost->categories as $one){
            $cats .= $one->name . ', ';
        }
        $cats = mb_substr($cats, 0, mb_strlen($cats)-2);
        return view('show_post', [
            'post' => $blogPost,
            'categories' => $cats,
        ]); //returns the view with the post
    }

    public function edit(Post $blogPost)
    {
        $cat_ids=[];
        foreach($blogPost->categories as $one){
            $cat_ids[] = $one->id;
        }
        return view('edit_post', [
            'post' => $blogPost,
            'is_active' => (($blogPost->active)?'checked':''),
            'cat_ids' => $cat_ids,
            'cats' => PostCategory::all(),
            ]); //returns the edit view with the post
    }

    
    public function update(Request $request, Post $blogPost)
    {
        $cat_ids = [];
        foreach ($request->category as $cid){
            $cat_ids[] = $cid;
        }        
        
        $blogPost->update([
            'name' => $request->title,
            'content' => $request->body,
            'active' => $request->has('is_active') ? true:false,
        ]);
        $blogPost->categories()->sync($cat_ids);

        return redirect('blog/' . $blogPost->id);
    }

    public function destroy(Request $request, $id)
    {
        $post = Post::find($id);
        $detach_cats = [];
        foreach($post->categories as $onecat){
            $detach_cats[] = $onecat->id;
        }
        $post->categories()->detach($detach_cats);
        $post->delete();
        return redirect('/home');
    }  
}
