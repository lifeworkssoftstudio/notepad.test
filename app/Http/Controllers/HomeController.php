<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function index()
    // {
    //   //fetch 5 posts from database which are active and latest
    //   $posts = Posts::where('active',1)->orderBy('created_at','desc')->paginate(5);
    //   //page heading
    //   $title = 'Latest Posts';
    //   //return home.blade.php template from resources/views folder
    //   return view('home')->withPosts($posts)->withTitle($title);
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->is_admin()){
            $posts = Post::all();
        }else {
            $where_arr=[
                ['author_id', Auth::id()],
                ['active', true],
            ];
            $posts = Post::where($where_arr)->get();
        }
        
        return view('home')->with('posts', $posts);
    }

    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_category()
    {
        if(Auth::user()->is_admin()){
            $cats = PostCategory::all();
            return view('home_category')->with('cats', $cats);
        }
    }    
}
