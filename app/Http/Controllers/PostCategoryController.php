<?php

namespace App\Http\Controllers;

use App\Models\PostCategory;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostCategoryController extends Controller
{
    public function create()
    {
        return view('create_category');
    }

    public function store(Request $request)
    {
        $newCategory = PostCategory::create([
            'name' => $request->title,
        ]);
        return redirect('home/categories');
    }

    public function show(PostCategory $blogCategory)
    {
        return view('show_category', [
            'category' => $blogCategory,
            'posts_assign' => count($blogCategory->posts)
        ]);
    }

    public function edit(PostCategory $blogCategory)
    {
        return view('edit_category', [
            'category' => $blogCategory,
            ]);
    }

    
    public function update(Request $request, PostCategory $blogCategory)
    {
        $blogCategory->update([
            'name' => $request->title,
        ]);
        return redirect('blog/category/' . $blogCategory->id);
    }

    public function destroy(Request $request, $id)
    {
        
        $blogCategory = PostCategory::where('id', $id)->first();

        $posts = $blogCategory->posts;
        $posts_loaded = [];
        foreach ($posts as $one){
            $posts_loaded[] = Post::with('categories')->where('id', $one->id)->first();
        }
        
        $default_category = PostCategory::where('id', 1)->first();
        foreach ($posts_loaded as $one){
            $one->categories()->attach([$default_category->id]);
            $one->save();
        }        

        foreach ($posts_loaded as $one){
            $one->categories()->detach([$blogCategory->id]);
            $one->save();
        }
        $blogCategory->delete();

        return redirect('home/categories');
    }
}
