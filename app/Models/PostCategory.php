<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
    // public function post()
    // {
    //     return $this->hasMany(Post::class);
    // }
    public function posts()
    {
        // return $this->hasMany(PostCategory::class)->with(Post::class);
        return $this->belongsToMany(Post::class, PostToCategory::class);
    }    
}
