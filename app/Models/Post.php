<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'author_id',
        'category_id',
        'active',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        // return $this->belongsToMany(PostCategory::class)->with(Post::class);
        return $this->belongsToMany(PostCategory::class, PostToCategory::class);
        // return $this->hasMany(PostCategory::class);
    }

    public function is_mine(Post $post)
    {
        return $post->author_id == Auth::id();
    }
}
